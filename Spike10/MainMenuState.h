#pragma once

#include "State.h"

class MainMenuState : public State
{
public:
	MainMenuState(std::vector<std::string> ids);
	~MainMenuState();

	std::string GetInput();

	void Handle(Command* cmd);
	void Display();
};

