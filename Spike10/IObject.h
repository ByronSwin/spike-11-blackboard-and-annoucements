#pragma once

#include "Message.h"


#include <string>
#include <vector>


class IObject
{
public:
	IObject(std::vector<std::string> ids);
	~IObject();

	std::string FirstId();

	virtual void Recieve(Message* toRec);

	bool AreYou(std::string id);

private:
	std::vector<std::string> _ids;
};

