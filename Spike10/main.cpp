#include <iostream>
#include <cstdlib>
#include "MessageSystem.h"
#include "GameMain.h"

using namespace std;

/*
	makes a game and tells it to run

*/
int main()
{
	
	GameMain* game = new GameMain();

	game->run();

	delete game;
	
	return 0;
}