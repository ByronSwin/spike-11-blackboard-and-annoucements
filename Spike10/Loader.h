#pragma once

#include "GameData.h"
#include "Location.h"
#include "Entity.h"
#include "Edge.h"
#include "ComponentLoader.h"

#include <vector>
#include <string>

#include <fstream>
#include <iostream>

class Loader
{
public:
	Loader(std::vector<std::string> adventures);

	void Load(std::string adventure, GameData* result);

	~Loader();
private:
	std::vector<std::string> SplitString(std::string input);

	std::vector<std::string> _adventures;
	
};

