#pragma once


#include <string>

class Message
{
public:
	Message(std::string sender, std::string to, std::string subject, std::string data);
	~Message();

	std::string Sender();
	std::string To();
	std::string Subject();
	std::string Data();

private:
	std::string _sender;
	std::string _to;
	std::string _subject;
	std::string _data;
};

