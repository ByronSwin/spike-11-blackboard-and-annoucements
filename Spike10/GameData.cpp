#include "GameData.h"


using namespace std;


/*
	Game Data class, handles all the data related to the main game. Manges the player, locations and the graph that connects them

	Uses a map for the connections, with a location as the key and a vector of edges as the value. This allows multiple connections per location that can be one way and 
	use any identifiers.
*/
GameData::GameData()
{
	_thePlayer = new Player();
}

/*
	Adds a location to data
	@param newLoc the location to add
*/
void GameData::AddLocation(Location* newLoc)
{
	_locations.push_back(newLoc);
}

/*
	Adds an edge to the map, using string to determine which location to link to.

	@param keyLoc the identifier of the location used as a key
	@param newEdge edge that is the new edge
*/
void GameData::AddEdge(string keyLoc, Edge* newEdge)
{
	for (Location* loc : _locations)
	{
		if (loc->AreYou(keyLoc))
		{
			_connections[loc].push_back(newEdge);
			break;
		}
	}
}

/*
	Tells the game to start, placing the player in whatever room is declared as the Start.
*/
void GameData::StartGame()
{
	for (Location* loc : _locations)
	{
		if (loc->AreYou("START"))
		{
			_thePlayer->SetLocation(loc);
			break;
		}
	}
}

/*
	Gets a location based on an identifier

	@param loc identifier of location. Returns null if no match
*/
Location* GameData::GetLoc(string loc)
{
	for (Location* l : _locations)
	{
		if (l->AreYou(loc))
		{
			return l;
		}
	}

	return NULL;
}

/*
	Gets a list of the current paths based ont the players current location

	@returns string the paths in the players current location
*/
string GameData::GetCurrPaths()
{
	string result = "";
	for (Edge* e : _connections[_thePlayer->GetLocation()])
	{
		result += "\n\t";
		result += e->FirstId();
	}

	return result;
}

/*
	Cheks if a path exists with the matching identifier, using the passed in location as key

	@param pathId identifier for the path
	@param loc the location to check on the graph
*/
bool GameData::HasPath(string pathId, Location* loc)
{
	bool result = false;
	if (_connections.count(loc) == 1)
	{
		for (Edge* e : _connections[loc])
		{
			if (e->AreYou(pathId))
			{
				result = true;
				break;
			}
		}
	}

	return result;
}

/*
	Moves the player to a matching path based on the identifier

	@param string identifier of the path. If not in current location, doesnt move player
*/
void GameData::MovePlayer(string pathId)
{
	if (_connections.count(_thePlayer->GetLocation()) == 1)
	{
		for (Edge* e : _connections[_thePlayer->GetLocation()])
		{
			if (e->AreYou(pathId))
			{
				_thePlayer->SetLocation(e->getOut());
			}
		}
	}
}

/*
	@returns the player
*/
Player* GameData::GetPlayer()
{
	return _thePlayer;
}

GameData::~GameData()
{
	delete _thePlayer;
}
