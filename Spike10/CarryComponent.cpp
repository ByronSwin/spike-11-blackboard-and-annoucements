#include "CarryComponent.h"

using namespace std;

/*
	A carry component, inherits from component.

	An entity with this componenet can be picked up by the player.
*/
CarryComponent::CarryComponent()
:Component({ "CARRYABLE" })
{
}

/*
	Returns the description of this component
*/
string CarryComponent::CompDesc()
{
	return "\n\tCarryable";
}

CarryComponent::~CarryComponent()
{
}
