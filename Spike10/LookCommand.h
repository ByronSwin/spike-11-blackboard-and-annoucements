#pragma once

#include "Command.h"

class LookCommand : public Command
{
public:
	LookCommand(std::vector<std::string> ids);
	void Update(std::vector<std::string> input);
	~LookCommand();

	std::string getAtId();
	std::string getInId();
	std::string getErrMsg();

private:
	std::string _atId;
	std::string _inId;
	std::string _errMsg;
};

