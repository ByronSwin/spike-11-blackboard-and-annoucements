#pragma once

#include "State.h"

#include <algorithm>
#include <fstream>
#include <iostream>

class InformationState : public State
{
public:
	InformationState(std::vector<std::string> ids);
	~InformationState();
	void Handle(Command* cmd);
	std::string GetInput();

	void Display();
};

