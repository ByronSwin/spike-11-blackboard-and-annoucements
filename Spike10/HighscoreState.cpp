#include "HighscoreState.h"

using namespace std;

/*
	A simple class used to make managing reading the file information easier
*/
class result
{
	string _name;
	int _score;
};

/*
	CURRENTLY NOT IMPLEMENTED
	State used for users to enter their highscore and have it saved to file
*/
HighscoreState::HighscoreState(vector<string> ids)
: State(ids)
{

}

/*
	Gets the users input

	@return string the users modifed unformatted input
*/
string HighscoreState::GetInput()
{
	string result = "HIGHSCORE ";
	string input;
	getline(cin, input);
	result += input;

	return result;
}

/*
	Handles the users command
*/
void HighscoreState::Handle(Command* cmd)
{
	
}

/*
	Prints to console this states relevant information
*/
void HighscoreState::Display()
{
	cout << "This is the highscore state" << endl;
}

HighscoreState::~HighscoreState()
{
}