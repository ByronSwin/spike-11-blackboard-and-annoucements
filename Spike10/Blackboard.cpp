#include "Blackboard.h"

using namespace std;

// A general defined failure message
static Message failed = Message("BLACKBOARD", "ONEWAY", "NO_MSG", "");

static map<std::string, Message*> _currMessages;

/**
	Access a message based on the parameter string subject.

	@param accessor the string subject to access

	@return returns the related message based on the subject, else a failed message.

*/
Message* Blackboard::Access(std::string accesor)
{
	for (map<string, Message*>::iterator targets = _currMessages.begin(); targets != _currMessages.end(); targets++)
	{
		if (targets->first.compare(accesor) == 0)
		{
			return targets->second;
		}
	}

	return &failed;
}

/**
	Adds a message, using its subject as the key
	NOTE: Overrites any message with the same subject

	@param newMsg the new message to post to the blackboard.
*/
void Blackboard::Add(Message* newMsg)
{
	_currMessages[newMsg->Subject()] = newMsg;
}

Blackboard::~Blackboard()
{
}
